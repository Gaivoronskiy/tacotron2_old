from glob import glob
import os
from sklearn.model_selection import train_test_split
from tqdm import tqdm

DIR_DATA = 'mozilla_data'


def convert(path_dir='mozilla', path_save='mozilla_data'):
    if not os.path.exists(os.path.join(path_save)): os.mkdir(os.path.join(path_save))
    path_save = os.path.join(path_save, 'wavs')
    if not os.path.exists(os.path.join(path_save)): os.mkdir(os.path.join(path_save))

    for fpath in tqdm(glob(os.path.join(path_dir, '**', '*.wav'))):
        os.system(f"ffmpeg -i {fpath} -loglevel quiet -ar 22050 -ac 1 {os.path.join(path_save, os.path.basename(fpath))}")
        os.remove(fpath)


def create_markup(path_data='mozilla', path_markup='filelists'):
    data = []
    for sample in glob(os.path.join(path_data, '**', '*.csv')):
        with open(sample, 'r', encoding='utf-8') as f:
            data += f.read().split('\n')[:-1]

    data = [os.path.join(DIR_DATA, 'wavs', sample) for sample in data]
    train, valid = train_test_split(data, test_size=0.1, random_state=42)

    with open(os.path.join(path_markup, 'ljs_audio_text_train_filelist.txt'), 'w', encoding='utf-8') as f:
        [f.write(sample + '\n') for sample in train]

    with open(os.path.join(path_markup, 'ljs_audio_text_val_filelist.txt'), 'w', encoding='utf-8') as f:
        [f.write(sample + '\n') for sample in valid]


if __name__ == '__main__':
    convert()
    create_markup()